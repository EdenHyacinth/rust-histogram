use frequency::Frequency;
use frequency_hashmap::HashMapFrequency;
use partition_methods::multiway_partition;
use partition_methods::partition_by_information;
use std::collections::HashSet;
use yet_another_data_frame::column_data::*;
use yet_another_data_frame::column_view::ColumnView;

use crate::Histogram;

#[derive(Clone, Debug)]
/// Inclusion is defined as being within the given HashSet.
pub struct CategoryHistogram {
    bin_boundaries: Vec<HashSet<usize>>,
    count_by_bin: Vec<usize>,
    bin_width: usize,
}

impl Histogram for CategoryHistogram {
    type Input = usize;
    type Bins = Vec<HashSet<usize>>;

    fn bin_boundaries(&self) -> Self::Bins {
        self.bin_boundaries.clone()
    }
    fn bin_counts(&self) -> Vec<usize> {
        self.count_by_bin.clone()
    }
    fn find_element_bin(&self, value: usize) -> usize {
        match self.bin_boundaries
            .iter()
            .position(|x| {
                x.contains(&value)
            }) {
            Some(bin) => bin,
            None => panic!("Value outside training set")
        }
    }
    fn len(&self) -> usize {
        self.bin_boundaries.len()
    }
}

impl CategoryHistogram {
    /// Category Histogram from Frequency
    fn create_frequency_boundaries(data: &ColumnData, indices: Vec<usize>) -> Vec<HashSet<usize>> {
        let category_frequency: HashMapFrequency<usize> = (*data).partial_accumulate(indices).into_iter().collect();
        let elem_keys: Vec<usize> = category_frequency.items().map(|x| *x).collect::<Vec<usize>>();
        let elem_counts = category_frequency.counts().map(|x| *x as f32).collect::<Vec<f32>>();
        let partitions =
            multiway_partition(elem_counts, (elem_keys.len() as f32).cbrt() as usize);

        partitions
            .into_iter()
            .map(|partition| {
                partition
                    .into_iter()
                    .map(|partition_index| elem_keys[partition_index])
                    .collect::<HashSet<usize>>()
            })
            .collect()
    }

    /// Category Histogram From Comparative Data
    fn create_comparative_boundaries(data: &ColumnData, indices: Vec<usize>, comparative_data: Vec<f32>) -> Vec<HashSet<usize>> {
        let partition_grouping = partition_by_information(
            (*data).partial_accumulate(indices).into_iter().collect(),
            comparative_data);

        partition_grouping
            .into_iter()
            .map(|grouping| {
                grouping.into_iter().collect::<HashSet<usize>>()
            }).collect::<Vec<HashSet<usize>>>()
    }
}
