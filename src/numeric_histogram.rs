use core::fmt;
use std::error;
use std::error::Error;
use std::ops::Div;

use num_traits::{Bounded, Float, Num, ToPrimitive};
use yet_another_data_frame::column_data::*;
use yet_another_data_frame::column_view::{ColumnView, ViewNonContiguousSlice};

use crate::{Histogram, ToHistogram};

const fn num_bits<T>() -> usize {
    std::mem::size_of::<T>() * 8
}

#[derive(Clone, Debug)]
/// Inclusion is defined as greater than the boundary, except in the case of the least most boundary.
pub struct NumericHistogram<T> {
    bin_boundaries: Vec<T>,
    count_by_bin: Vec<usize>,
    bin_width: T,
}

fn from_numeric_column_data_internal<T>(data: &ColumnData, indices: Vec<usize>) -> NumericHistogram<T>
    where
        T: Num + Float + Sized + ToPrimitive,
        T: std::ops::AddAssign + Bounded + PartialOrd,
        for<'a> ColumnView<'a, T>: ViewNonContiguousSlice<'a> + Iterator<Item=T>,
{
    let standard_deviation: T = data.standard_deviation(indices.clone());
    let bounds: (T, T) = (data.min(indices.clone()), data.max(indices.clone()));
    let (bin_width, est_bins) =
        NumericHistogram::bin_properties(indices.len(), bounds.clone(), standard_deviation);
    let boundaries: Vec<T> = (1_usize..=est_bins)
        .map(|i| bounds.0 + bin_width * T::from(i - 1).unwrap())
        .collect();
    let (_bin_by_row, bin_frequency) =
        NumericHistogram::bin_frequency(&data, &boundaries, indices, est_bins);

    NumericHistogram::<T> {
        bin_boundaries: boundaries,
        count_by_bin: bin_frequency,
        bin_width,
    }
}

impl<T> NumericHistogram<T>
    where
        T: Num + Float + Sized + ToPrimitive,
        T: std::ops::AddAssign + Bounded + PartialOrd,
        for<'a> ColumnView<'a, T>: ViewNonContiguousSlice<'a> + Iterator<Item=T>,
{
    fn bin_properties(indices_length: usize, bounds: (T, T), standard_deviation: T) -> (T, usize)
        where
            T: Num + Float + Copy,
    {
        if indices_length < 20 {
            // Sturges Formula for Bin Selection - as selection is unlikely to be normally
            // distributed at this granularity
            let est_bins: usize =
                1_usize + (num_bits::<usize>() as u32 - indices_length.leading_zeros()) as usize;
            let bin_width: T =
                (bounds.1 - bounds.0) / T::from(est_bins).unwrap() + Float::epsilon();
            (bin_width, est_bins)
        } else {
            // With likely randomly selected normally distributed data, we can use Scott's normal
            // reference rule
            let bin_width = standard_deviation * T::from(3.5).unwrap()
                / (T::from(indices_length).unwrap()).cbrt();
            let est_bins: usize =
                ToPrimitive::to_u32(&(bounds.1 - bounds.0 / bin_width).floor()).unwrap() as usize;
            (bin_width, est_bins)
        }
    }
    fn find_bucket(data: &[T], elem: T) -> usize
        where
            T: Num + Float + ToPrimitive,
    {
        ToPrimitive::to_u32(&(elem / data[1] - T::from(0.01_f32).unwrap())).unwrap() as usize
    }

    fn bin_frequency(
        column_data: &ColumnData,
        boundaries: &[T],
        indices: Vec<usize>,
        bins: usize,
    ) -> (Vec<usize>, Vec<usize>)
        where
            T: Num + Float,
            for<'a> ColumnView<'a, T>: ViewNonContiguousSlice<'a> + Iterator<Item=T>,
    {
        let mut bin_frequency: Vec<usize> = vec![0_usize; bins];
        let mut bin_by_row: Vec<usize> = Vec::with_capacity(column_data.data_len);

        for element in column_data.view_column::<ColumnView<'_, T>>(indices) {
            let histogram_bin = Self::find_bucket(&boundaries, element);
            bin_by_row.push(histogram_bin);
            bin_frequency[histogram_bin] += 1_usize
        }
        (bin_by_row, bin_frequency)
    }
}

impl<T> Histogram for NumericHistogram<T>
    where
        T: Num + Float + Sized + ToPrimitive,
        T: std::ops::AddAssign + Bounded + PartialOrd,
        for<'a> ColumnView<'a, T>: ViewNonContiguousSlice<'a> + Iterator<Item=T>,
        Vec<T>: Clone,
{
    type Input = T;
    type Bins = Vec<T>;

    fn bin_boundaries(&self) -> Self::Bins {
        self.bin_boundaries.clone()
    }
    fn bin_counts(&self) -> Vec<usize> {
        self.count_by_bin.clone()
    }
    fn find_element_bin(&self, value: T) -> usize {
        Self::find_bucket(&self.bin_boundaries, value)
    }
    fn len(&self) -> usize {
        self.bin_boundaries.len()
    }
}

impl<'a> ToHistogram<'a> for NumericHistogram<f32> {
    fn from_column_data(data: &'a ColumnData, indices: Vec<usize>) -> Option<Self> {
        match &(*data).data {
            ColumnType::Float(_) => {
                Some(from_numeric_column_data_internal::<f32>(data, indices))
            },
            _ => None
        }
    }
}

#[cfg(test)]
mod tests {
    use yet_another_data_frame::column_data::{ColumnData, ColumnType};

    use crate::{Histogram, numeric_histogram::NumericHistogram};
    use crate::ToHistogram;

    #[test]
    fn create_histogram() {
        let c_data: ColumnData = ColumnData {
            data: ColumnType::Float(vec![
                0.0_f32, 1.0, 20.0, 3.0, 4.0, 15.5, 16.4, 7.0, 7.6, 7.9, 4.6, 4.5,
            ]),
            data_len: 10_usize,
        };
        let hist = NumericHistogram::from_column_data(
            &c_data,
            vec![0_usize, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
        )
            .unwrap();
        dbg!(hist.bin_boundaries());
        dbg!(hist.bin_counts());
    }

    #[test]
    fn single_value() {
        let c_data: ColumnData = ColumnData {
            data: ColumnType::Float(vec![
                0.0_f32, 1.0, 20.0, 3.0, 4.0, 15.5, 16.4, 7.0, 7.6, 7.9, 4.6, 4.5,
            ]),
            data_len: 10_usize,
        };
        let hist = NumericHistogram::from_column_data(
            &c_data,
            vec![0_usize, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
        )
            .unwrap();
        assert_eq!(hist.find_element_bin(4.0), 0_usize);
        assert_eq!(hist.find_element_bin(4.1), 1_usize);
        assert_eq!(hist.find_element_bin(0.0), 0_usize);
    }

    #[test]
    fn single_value_no_zero_bound() {
        let c_data: ColumnData = ColumnData {
            data: ColumnType::Float(vec![
                1.0_f32, 1.0, 20.0, 3.0, 4.0, 15.5, 16.4, 7.0, 7.6, 7.9, 4.6, 4.5,
            ]),
            data_len: 12_usize,
        };
        let hist = NumericHistogram::from_column_data(
            &c_data,
            vec![0_usize, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
        )
            .unwrap();
        assert_eq!(hist.find_element_bin(4.8), 0_usize);
        assert_eq!(hist.find_element_bin(4.9), 1_usize);
        assert_eq!(hist.find_element_bin(1.0), 0_usize);
    }

    #[test]
    fn create_histogram_scotts() {
        let c_data: ColumnData = ColumnData {
            data: ColumnType::Float(vec![
                1.0_f32, 1.0, 20.0, 3.0, 4.0, 15.5, 16.4, 7.0, 7.6, 7.9, 4.6, 4.5, 1.0_f32, 1.0,
                20.0, 3.0, 4.0, 15.5, 16.4, 7.0, 7.6, 7.9, 4.6, 4.5, 1.0_f32, 1.0, 20.0, 3.0, 4.0,
                15.5, 16.4, 7.0, 7.6, 7.9, 4.6, 4.5,
            ]),
            data_len: 30_usize,
        };
        let hist = NumericHistogram::from_column_data(
            &c_data,
            vec![
                0_usize, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
            ],
        )
            .unwrap();
        assert_eq!(hist.find_element_bin(4.8), 0_usize);
    }
}
