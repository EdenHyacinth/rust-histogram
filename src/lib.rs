extern crate frequency;
extern crate frequency_hashmap;
extern crate num_traits;
extern crate yet_another_data_frame;

use yet_another_data_frame::column_data::{ColumnData, ColumnType};

use crate::category_histogram::CategoryHistogram;
use crate::numeric_histogram::NumericHistogram;

pub mod category_histogram;
pub mod numeric_histogram;

pub trait Histogram {
    type Input;
    type Bins;

    fn bin_boundaries(&self) -> Self::Bins;
    fn bin_counts(&self) -> Vec<usize>;
    fn find_element_bin(&self, value: Self::Input) -> usize;
    fn len(&self) -> usize;
    fn is_empty(&self) -> bool {
        self.len() == 0_usize
    }
}

trait ToHistogram<'a>
    where
        Self: Sized,
{
    fn from_column_data(data: &'a ColumnData, indices: Vec<usize>) -> Option<Self>;
}